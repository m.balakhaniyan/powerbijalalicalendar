"use strict";

import "core-js/stable";
import "./../style/visual.less";
import "./../style/style.css";
import "./../style/Slider.css";
import powerbi from "powerbi-visuals-api";
import * as $ from 'JQuery';
import {VisualSettings} from "./settings";
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstance = powerbi.VisualObjectInstance;
import DataView = powerbi.DataView;
import VisualObjectInstanceEnumerationObject = powerbi.VisualObjectInstanceEnumerationObject;
import data = powerbi.data;

class Slider {
    private readonly input: JQuery;
    private readonly parent: JQuery;
    private readonly holder: JQuery;
    private readonly leftHead: JQuery;
    private readonly rightHead: JQuery;
    private readonly body: JQuery;
    private readonly head: JQuery;
    private static headsClass: string[] = ['slider-head', 'slider-left-head', 'slider-right-head'];
    private whichHeadMustMove: number = 0;
    private readonly range: number[];
    private readonly prettify = (x, y) => {
        return [x, y]
    };
    private readonly onChange = (x, y) => {
        return [x, y]
    }
    private readonly onStart = (x, y) => {
        return [x, y]
    }
    private readonly onUpdate = (x, y) => {
        return [x, y]
    }

    setBubbles(data: number[]) {
        this.leftHead.find('.bubble').text(data[0]);
        this.rightHead.find('.bubble').text(data[1]);
    }

    private setNumbers() {
        let setPercent = (head: JQuery) => {
            return Math.round((Math.abs(head.offset().left - this.holder.offset().left) / (this.holder.width())) * (this.range[1] - this.range[0]) + this.range[0])
        }
        return [setPercent(this.leftHead), setPercent(this.rightHead)];
    }

    private getNumbersForBubble() {
        let data = this.setNumbers();
        this.setBubbles(this.prettify(data[0], data[1]));
        this.onChange(data[0], data[1]);
    }

    private headMoved() {
        if (this.whichHeadMustMove !== 0) {
            let data = this.setNumbers();
            this.onUpdate(data[0], data[1]);
            this.getNumbersForBubble();
        }
        this.whichHeadMustMove = 0;

    }

    private moveBody() {
        let headWidth: number = 2;
        let leftHeadLeft = parseInt(this.leftHead.css('left')) + headWidth;
        let rightHeadLeft = parseInt(this.rightHead.css('left'));
        this.body.css('left', (leftHeadLeft / this.holder.width() * 100).toString() + '%');
        this.body.css('width', ((rightHeadLeft - leftHeadLeft + headWidth - 1) / this.holder.width() * 100).toString() + '%');
    }

    private setHeadPosWithClick(ox: number, whichHead = 0) {
        let holderOffsetLeft: number = this.holder.offset().left;
        ox = ox - holderOffsetLeft;
        let distances: number[] = [Math.abs(this.leftHead.offset().left - ox), Math.abs(this.rightHead.offset().left - ox)];
        let pxPercent: string = (ox / (this.holder.width()) * 100).toString() + '%';
        if (whichHead === 0) {
            if (distances[0] < distances[1])
                this.leftHead.css('left', pxPercent)
            else
                this.rightHead.css('left', pxPercent)
        } else if (whichHead === 1) {
            this.leftHead.css('left', pxPercent)
        } else {
            if (this.leftHead.offset().left <= ox + this.holder.offset().left)
                this.rightHead.css('left', pxPercent)
        }
        this.moveBody();
        this.getNumbersForBubble();
        let data = this.setNumbers();
        this.onUpdate(data[0], data[1]);
    }

    private setHeadPosWithDrag(px) {
        let holderOffsetLeft: number = this.holder.offset().left;
        if (this.whichHeadMustMove != 0 && (Math.floor(px) <= Math.floor(holderOffsetLeft) || Math.floor(holderOffsetLeft + this.holder.width()) <= Math.floor(px))) {
            this.headMoved();
            return
        }
        px = px - holderOffsetLeft;
        let pxPercent: string = (px / (this.holder.width()) * 100).toString() + '%';
        if (this.whichHeadMustMove === 1 && px <= this.rightHead.offset().left - holderOffsetLeft) {
            this.leftHead.css('left', pxPercent);
            this.getNumbersForBubble();
        } else if (this.whichHeadMustMove === 2 && px >= this.leftHead.offset().left - holderOffsetLeft) {
            this.rightHead.css('left', pxPercent);
            this.getNumbersForBubble();
        }
    }

    constructor(option: object) {
        let _self = this;
        if (option['prettify'] !== undefined)
            this.prettify = option['prettify'];
        if (option['onChange'] !== undefined)
            this.onChange = option['onChange'];
        if (option['onStart'] !== undefined)
            this.onStart = option['onStart'];
        if (option['onUpdate'] !== undefined)
            this.onUpdate = option['onUpdate'];
        this.range = option['range'];
        this.input = $(`#${option['input']}`);
        this.input.css('display', 'none');
        this.input.wrap('<div class="slider-parent"></div>');
        this.parent = this.input.parent();
        this.parent.append('<div class="slider-holder"><span class="legend"></span><span class="legend"></span><span class="legend"></span><span class="legend"></span><span class="legend"></span></div>');
        this.holder = this.parent.find('.slider-holder');
        this.holder.append('<div class="slider-head slider-left-head"><div class="bubble"></div></div><div class="slider-body"></div><div class="slider-head slider-right-head"><div class="bubble"></div></div>');
        this.leftHead = this.holder.find('.slider-left-head');
        this.rightHead = this.holder.find('.slider-right-head');
        this.rightHead.css('left', '50%');
        this.body = this.holder.find('.slider-body');
        this.head = this.holder.find('.slider-head');
        this.moveBody();
        this.head.on('mousedown', function () {
            _self.whichHeadMustMove = $(this).hasClass(Slider.headsClass[1]) ? 1 : 2;
        })

        $('html').on('mousemove', function (e) {
            _self.setHeadPosWithDrag(e.pageX);
            _self.moveBody();
        }).on('mouseup', function () {
            _self.headMoved();
        });
        this.holder.on('click', function (e) {
            if (!$(e.target).hasClass(Slider.headsClass[0])) {
                _self.setHeadPosWithClick(e.pageX);
            }
        });
        let data = this.setNumbers();
        let i = 0, number;
        this.holder.find('.legend').each(function () {
            number = Math.round(i * (_self.range[1] - _self.range[0]) / 100) + _self.range[0];
            $(this).append(`|<br><span>${_self.prettify(number, 1)[0]}</span>`);
            i += 25;
        });
        let initialsValues = option['initialValues'];
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[0]) * (this.holder.width()) + this.holder.offset().left, 1);
        this.setHeadPosWithClick(this.setFromToForPos(initialsValues[1]) * (this.holder.width()) + this.holder.offset().left, 2);
        this.onStart(data[0], data[1]);
    }

    private setFromToForPos(num) {
        return (parseInt(num) - this.range[0]) / (this.range[1] - this.range[0])
    }

    public setSliderChanger(params) {
        let _self = this;
        let from, to;
        if (params['from'] != undefined) {
            from = _self.setFromToForPos((params['from'] + 24 * 60 * 60 * 100));
            if (from >= 0)
                _self.setHeadPosWithClick(from * (this.holder.width()) + this.holder.offset().left, 1)
        }
        if (params['to'] != undefined) {
            to = _self.setFromToForPos((params['to'] + 24 * 60 * 60 * 100));
            if (to <= 1)
                _self.setHeadPosWithClick(to * (this.holder.width()) + this.holder.offset().left, 2)
        }
    }
}

class PersianFormat {
    public static readonly _jalali_months: string[] = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
    private static readonly _persianNumbers: RegExp[] = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
    private static readonly _onlyPersianNumbers: string[] = PersianFormat._persianNumbers.map((x: RegExp) => {
        return x.toString().substr(1, 1)
    });
    public static readonly jal_month_days: number[] = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];

    public static isLeapYear(year: number): boolean {
        let remain_nums = [0, 4, 8, 12, 16, 20, 24, 29, 33, 37, 41, 45, 49, 53, 57, 62, 66, 70, 74, 78, 82, 86, 90, 95, 99, 103, 107, 111, 115, 119, 124]
        return remain_nums.indexOf(year % 128) !== -1;
    }

    public static fixNumbers(str: string, cmd: number = 0): string {
        if (cmd === 0)
            for (let i = 0; i < 10; i++)
                str = str.replace(PersianFormat._persianNumbers[i], i.toString());
        else
            for (let i = 0; i < 10; i++)
                str = str.split(i.toString()).join(PersianFormat._onlyPersianNumbers[i]);
        return str;
    };

    public static getMinMaxDates(dates) {
        let data = dates.map(function (x) {
            return PersianFormat.getTimestampFromStandardPersian(x);
        })
        return [(dates[data.indexOf(Math.min(...data))]), (dates[data.indexOf(Math.max(...data))])];
    }

    public static setDatesStringStandardArrays(dates, separator: string = '/') {
        return dates.map(function (x) {
            return x.split(separator).map(function (y) {
                return parseInt(y)
            }).reverse();
        })
    }

    public static getTimestampFromInput(date) {
        let datePersian = (date.toString().split(' '));
        datePersian = [PersianFormat.fixNumbers(datePersian[0]), (PersianFormat._jalali_months.indexOf(datePersian[1])).toString(), PersianFormat.fixNumbers(datePersian[2])]
        let dateJalali = (datePersian.map(function (x) {
            return parseInt(x)
        }));
        return (PersianFormat.dateToGre(dateJalali.reverse()).valueOf())
    }

    public static getTimestampFromStandardPersian(date: string, separator: string = '/') {
        let dateArr: string[] | number[] = date.split(separator);
        dateArr = dateArr.map(function (x) {
            return parseInt(x)
        }).reverse();
        dateArr[1] -= 1;
        return PersianFormat.dateToGre(dateArr).valueOf();
    }

    public static dateToGre(date: number[]): Date {
        let [y, m, d] = date;
        let gy = y + 621, gm, gd;
        gy = (m >= 10 && d >= 11) ? gy + 1 : gy;
        if (m === 11 && 1 <= d && d <= 10) {
            gy++;
        } else if (m == 10 && d == 11) {
            gy--;
        }
        let month_floor = [11, 10, 10, 9, 9, 9, 8, 9, 9, 10, 11, 9];
        if (d > month_floor[m]) {
            gm = (m + 3) % 12;
            gd = (d - month_floor[m])
        } else {
            gm = (m + 2) % 12;
            gd = d + PersianFormat.jal_month_days[(m === 0) ? 11 : m - 1] - (month_floor[(m === 0) ? 11 : m - 1]);
        }
        gm = (gm == 0) ? 12 : gm;
        if (PersianFormat.isLeapYear(y)) {
            gd -= 1;
        }
        return new Date(gy, gm, gd);
    }

    public static dateToTS(y: number, m: number, d: number): number {
        return PersianFormat.dateToGre([y, m, d]).valueOf();
    }

    public static tsToDate(date: number): string {
        let loc = "fa-IR"
        let d = new Date(date);
        return d.toLocaleDateString(loc, {
            day: 'numeric',
            month: 'long',
            year: 'numeric'
        });
    }
}

class Calendar {
    private readonly elem: JQuery;
    private readonly holder: JQuery;
    private next: JQuery;
    private prev: JQuery;
    private year: JQuery;
    private month: JQuery;
    private today: JQuery;
    private thead: JQuery;
    private tbody: JQuery;
    private readonly fromTo: number[];
    private readonly sliderHeader: string;
    private readonly slider: Slider;

    setCalendar(): void {
        this.holder.append(`<div class="calendar"></div>`)
    }

    private addCalendarParts() {
        this.setCalendar();
        this.holder.find('.calendar').append(`<div class="header">
                <span class="change prev">قبلی</span>
                <span class="select-holder"><select class="year"></select><select class="month"></select></span>
                <span class="change next">بعدی</span>
                </div><div class="body"><table><thead></thead><tbody></tbody></table></div><div class="footer today">امروز</div>`);
        this.next = this.holder.find('.next');
        this.prev = this.holder.find('.prev');
        this.year = this.holder.find('.year');
        this.month = this.holder.find('.month');
        this.today = this.holder.find('.today');
        this.thead = this.holder.find('thead');
        this.tbody = this.holder.find('tbody');
    }

    private addMonthYearData() {
        for (let month of PersianFormat._jalali_months) {
            this.month.append(`<option value="${month}">${month}</option>`)
        }
        for (let i = this.fromTo[0]; i <= this.fromTo[1]; i++) {
            this.year.append(`<option value="${i}">${PersianFormat.fixNumbers((i).toString(), 1)}</option>`)
        }
        this.month.find('.option').css('visibility', 'visible')
        this.year.find('.option').css('visibility', 'visible')
    }

    private setTableRows() {
        for (let day_name of ['', 'یک', 'دو', 'سه ', 'چهار', 'پنج', 'جمعه'].reverse()) {
            this.thead.append(`<td>${(day_name == 'جمعه') ? day_name : day_name + 'شنبه'}</td>`);
        }
        for (let j = 1; j <= 6; j++) {
            let tr = $(`<tr></tr>`)
            for (let i = 1; i <= 7; i++) {
                tr.append(`<td class="${(i == 1) ? "weekend" : ""}"><span class="jalali"></span><span class="gregorian"></span></td>`);
            }
            this.tbody.append(tr);
        }
    }

    showCalendar() {
        let elem_date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        this.month.val(elem_date[1]);
        this.year.val(elem_date[2]);
    }

    setMonthYear() {
        this.updateTable();
        $(this).find('option').css('visibility', 'visible')
    }

    updateTable() {
        let getGregorianDate = (day) => {
            return PersianFormat.dateToGre([this_year, mon_index, day]).getDate().toString()
        }
        let this_month = this.month.val().toString();
        let this_year = parseInt(this.year.val().toString());
        let mon_index = PersianFormat._jalali_months.indexOf(this_month);
        let g_date = PersianFormat.dateToGre([(this_year), mon_index, 1]);
        let weekday = g_date.getDay();
        let j = 0, i = 1;
        let m_days = PersianFormat.jal_month_days[PersianFormat._jalali_months.indexOf(this_month)];
        weekday = ((weekday + 1) % 7);
        let isLeap = PersianFormat.isLeapYear(this_year);
        this.tbody.find('tr').each(function () {
                let tds = $(this).find('td');
                for (let k = 0; k < 7; k++) {
                    if (i <= m_days || (mon_index === 11 && isLeap && i === 30)) {
                        if (j >= weekday) {
                            $(tds[6 - k]).css('visibility', 'visible')
                                .attr('data-date', i).find('.jalali')
                                .text(PersianFormat.fixNumbers((i).toString(), 1))
                                .parent().find('.gregorian')
                                .text(getGregorianDate(i++));
                        } else $(tds[6 - k]).css('visibility', 'hidden');
                    } else $(tds[6 - k]).css('visibility', 'hidden');
                    j++;
                }
            }
        );
        this.holder.find('td.today').removeClass('today');
        this.holder.find('td.selected-day').removeClass('selected-day');
        this.setToday(1);
        setTimeout(() => {
            this.selectedDay();
        }, 10);
    }

    nextPrev(cmd = 0) {
        let _self = this;
        let month_num = (PersianFormat._jalali_months.indexOf(_self.month.val().toString()));
        let year_num = parseInt(_self.year.val().toString());
        if (cmd == 1) {
            if (month_num === 11) {
                if (year_num !== this.fromTo[1]) {
                    _self.year.val((year_num + 1).toString());
                    _self.month.val(PersianFormat._jalali_months[0]);
                }
            } else
                _self.month.val(PersianFormat._jalali_months[month_num + 1]);
        } else {
            if (month_num === 0) {
                if (year_num != this.fromTo[0]) {
                    _self.year.val((year_num - 1).toString());
                    _self.month.val(PersianFormat._jalali_months[11]);
                }
            } else
                _self.month.val(PersianFormat._jalali_months[month_num - 1]);
        }
        _self.holder.find('td').removeClass('today');
        _self.updateTable();
        _self.selectedDay();
    }

    setToday(cmd = 0) {
        let today = (PersianFormat.fixNumbers(new Date().toLocaleDateString('fa-IR')).split('/'))
        if (cmd === 0) {
            this.month.val(PersianFormat._jalali_months[parseInt(today[1]) - 1]);
            this.year.val(today[0]);
            this.updateTable();
        }
        let calDate = [this.year.val().toString(), (PersianFormat._jalali_months.indexOf(this.month.val().toString()) + 1).toString()]
        setTimeout(() => {
            if (today[0] === calDate[0] && today[1] === calDate[1])
                this.tbody.find(`td[data-date=${today[2]}]`).addClass('today');
        }, 10);
    }


    selectedDay() {
        let sel_year = parseInt((this.year.val()).toString());
        let sel_month = PersianFormat._jalali_months.indexOf((this.month.val()).toString());
        let date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        if (sel_month === PersianFormat._jalali_months.indexOf(date[1]) && sel_year === parseInt(date[2])) {
            setTimeout(() => {
                this.tbody
                    .find(`td[data-date=${date[0]}]`).addClass('selected-day');
            }, 10);
        }
    }

    updateSlider(day) {
        let month = PersianFormat._jalali_months.indexOf(this.month.val().toString());
        let year = parseInt(this.year.val().toString());
        if (this.sliderHeader === 'start') {
            this.slider.setSliderChanger({
                from: PersianFormat.dateToTS(year, month, day)
            })
        } else if (this.sliderHeader === 'end') {
            this.slider.setSliderChanger({
                to: PersianFormat.dateToTS(year, month, day)
            })
        } else {
            this.elem.val([(PersianFormat.fixNumbers(day.toString(), 1)), PersianFormat._jalali_months[month], PersianFormat.fixNumbers(year.toString(), 1)].join(' '))
        }
    }

    createCalendar() {
        let _self = this;
        this.elem.on('click', () => {
            this.showCalendar()
        })
        this.month.on('change', () => {
            this.setMonthYear();
        });
        this.year.on('change', () => {
            this.setMonthYear()
        });
        this.prev.on('click', () => {
            this.nextPrev();
        });
        this.next.on('click', () => {
            this.nextPrev(1);
        });
        this.today.on('click', function () {
            _self.setToday();
        })
        this.setTableRows();
        this.holder.find('td').on('click', function () {
            let day = parseInt($(this).attr('data-date'));
            _self.tbody.find('td.selected-day').removeClass('selected-day')
            _self.updateSlider(day);
            _self.selectedDay();
        });
    }

    constructor(option: object) {
        let _self = this
        this.sliderHeader = option['header'];
        this.slider = option['slider'];
        this.elem = $(`#${option['id']}`);
        let initialDate: string = [PersianFormat._jalali_months[option['initial_date'][1]], PersianFormat.fixNumbers(option['initial_date'][0].toString(), 1), PersianFormat.fixNumbers(option['initial_date'][2].toString(), 1)].join(' ')
        let initialDateArray: string[] = initialDate.split(' ');
        initialDate = [initialDateArray[2], initialDateArray[0], initialDateArray[1]].join(' ');
        this.elem
            .addClass('date-input')
            .attr('value', initialDate)
            .prop('readonly', true)
            .wrap('<div class="date-input-holder"></div>');
        this.holder = this.elem.parent();
        this.addCalendarParts();
        this.elem.on('click', () => {
            _self.updateTable();
            this.elem.parent().find('.calendar').css('visibility', 'visible');
        })
        $("html").on('click', function (e) {
            if ($(e.target).closest(_self.holder).length === 0) {
                if ((_self.holder.find('.calendar').css('visibility')) == 'visible') {
                    _self.holder.find('.calendar').css('visibility', 'hidden')
                        .find('tbody td').css('visibility', 'hidden');
                }
            }
        });

        this.fromTo = [option['from'], option['to']]
        this.createCalendar();
        this.addMonthYearData();
    }
}

class PBReport {
    constructor(option: object) {
        $(`#${option['main']}`).append('<br><br><br><input id="inp"><br><div class="date-input-holder"><input id="start"/><br><input id="end"/></div>');
        let slider = new Slider({
            input: 'inp',
            range: [PersianFormat.dateToTS(option['from'][0], option['from'][1] - 1, option['from'][2]), PersianFormat.dateToTS(option['to'][0], option['to'][1] - 1, option['to'][2] + 1)],
            initialValues: [PersianFormat.dateToTS(option['initial_from'][0], option['initial_from'][1] - 1, option['initial_from'][2] + 1), PersianFormat.dateToTS(option['initial_to'][0], option['initial_to'][1] - 1, option['initial_to'][2] + 1)],
            prettify: function (x, y) {
                return [PersianFormat.tsToDate(x), PersianFormat.tsToDate(y)];
            },
            onChange: function (x, y) {
                $("#start").attr('value', PersianFormat.tsToDate(x));
                $("#end").attr('value', PersianFormat.tsToDate(y));
            },
        });
        option['initial_from'][1] -= 1;

        new Calendar({
            id: 'start',
            initial_date: option['initial_from'],
            from: option['from'][0],
            to: option['to'][0],
            slider: slider,
            header: 'start'
        });
        option['initial_to'][1] -= 1;
        new Calendar({
            id: 'end',
            initial_date: option['initial_to'],
            from: option['from'][0],
            to: option['to'][0],
            slider: slider,
            header: 'end'
        });
    }
}


export class Visual implements IVisual {
    private target: HTMLElement;
    private updateCount: number;
    private settings: VisualSettings;
    private first_state: boolean = true;
    private dateIndex: number = 100000;
    private options: VisualUpdateOptions;

    constructor(options: VisualConstructorOptions) {
        this.target = options.element;
        this.updateCount = 0;
        if (document) {
            const main: HTMLElement = document.createElement("div");
            main.setAttribute('id', 'main')
            this.target.appendChild(main);
        }
    }

    public updateTable() {
        let tbl = $('<table class="data-show"></table>')
        let cate = this.options.dataViews[0].categorical.categories;
        for (let u = 0; u < cate.length; u++) {
            if (cate[u].values[0].toString().split('/').length === 3) {
                this.dateIndex = u;
                break;
            }
        }
        if (cate.length >= this.dateIndex + 1) {
            if (this.first_state) {
                let vals = (cate[this.dateIndex].values.map(function (x) {
                    return x.toString();
                }));
                let removeNonDates = (arr) => {
                    let indexes = []
                    arr.forEach(function (v, i) {
                        if (v.split('/').length !== 3)
                            indexes.push(i);
                    })
                    for (let i of indexes) {
                        arr.splice(i, 1);
                    }
                    return arr;
                }
                vals = removeNonDates(vals)
                let minMax = PersianFormat.setDatesStringStandardArrays(PersianFormat.getMinMaxDates(vals))
                new PBReport({
                    main: "main",
                    from: minMax[0],
                    to: minMax[1],
                    initial_from: minMax[0],
                    initial_to: minMax[1],
                });
                this.first_state = false;
            }
            $('#main table.data-show').remove();
            let tr: JQuery;
            let from = PersianFormat.getTimestampFromInput($('#start').val())
            let to = PersianFormat.getTimestampFromInput($('#end').val())
            for (let i = 0; i < cate[0].values.length; i++)
                if (from <= PersianFormat.getTimestampFromStandardPersian(cate[this.dateIndex].values[i].toString()) && to >= PersianFormat.getTimestampFromStandardPersian(cate[this.dateIndex].values[i].toString())) {
                    tr = $(`<tr></tr>`);
                    for (let j = 0; j < cate.length; j++) {
                        tr.append(`<td>${cate[j].values[i].toString()}</td>`)
                    }
                    tbl.append(tr)
                }
            $('#main').append(tbl);
        } else
            return;
    }

    public update(options: VisualUpdateOptions) {
        this.settings = Visual.parseSettings(options && options.dataViews && options.dataViews[0]);
        this.options = options;
        this.updateTable();
    }

    private static parseSettings(dataView: DataView): VisualSettings {
        return <VisualSettings>VisualSettings.parse(dataView);
    }

    public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
        return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
    }
}